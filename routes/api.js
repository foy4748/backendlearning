const express = require("express");
const jwtDecoder = require("jwt-decode");
const cookieJwtAuth = require("../middlewares/cookieJwtAuth");
const router = express.Router();

const PAYMENT = require("../models/PAYMENT");

router.get("/", cookieJwtAuth, async (req, res) => {
  const token = req.cookies.token;
  const { userId } = await jwtDecoder(token);
  const some_JSON = await PAYMENT.find({ user: userId });
  res.send(some_JSON);
});

module.exports = router;
