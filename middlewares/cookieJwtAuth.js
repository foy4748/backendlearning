const jwt = require("jsonwebtoken");

const cookieJwtAuth = (req, res, next) => {
  const token = req.cookies.token;
  try {
    const user = jwt.verify(token, process.env.sessionSecret);
    req.user = user;
    next();
  } catch (error) {
    res.clearCookie("token");
    console.log("User is not Logged in.\n");
    return res.redirect("/userLogin");
  }
};

module.exports = cookieJwtAuth;
