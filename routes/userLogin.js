//userLogin page handler
const express = require("express");
const path = require("path");
const router = express.Router();
const jwt = require("jsonwebtoken");

//Importing necessary modules
const bcrypt = require("bcrypt");

//Importing Mongoose model
const USER = require("../models/USER");

router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "/userLogin.html"));
});

router.post("/", async (req, res) => {
  try {
    USER.findOne({ username: req.body.username }, (error, result) => {
      if (error) {
        console.log(`Something went wrong: \n ${error}`);
      }

      if (!result || !bcrypt.compareSync(req.body.password, result.password)) {
        res.send(
          "username / password is INVALID!!! or Account doesn't exist. Go <a href=/userRegistration>here</a> for registration"
        );
      } else {
        //Generate Token Here
        const token = jwt.sign(
          {
            //PayLoad
            username: result.username,
            userId: result._id,
          },
          //SALT
          process.env.sessionSecret,
          {
            //Options
            expiresIn: "1h",
          }
        );
        res.cookie("token", token, { httpOnly: true });
        res.send(
          `Welcome aboard ${result.username}.<a href="/payment"> Add your payment </a>`
        );
      }
    });
  } catch (err) {
    console.log("Something went wrong \n", err);
  }
});

module.exports = router;
