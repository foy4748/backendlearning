//payment page handler
const express = require("express");
const router = express.Router();

router.get("/", (req, res) => {
  res.cookie("token", { expiresIn: 0 }).send("You have been logged out");
  res.end();
});

module.exports = router;
