//Importing Modules
require("./dbConnect");
const express = require("express");
const cors = require("cors");
const path = require("path");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

const app = express(); //Executing Express App
const port = process.env.PORT || 3001;

//App is using these middlewares
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

//App is importing these routes
const api = require("./routes/api");
const userRegistration = require("./routes/userRegistration");
const userLogin = require("./routes/userLogin");
const payment = require("./routes/payment");
const logout = require("./routes/logout");

//App is using these routes
app.use("/api", api);
app.use("/userRegistration", userRegistration);
app.use("/userLogin", userLogin);
app.use("/payment", payment);
app.use("/logout", logout);

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./index.html"));
});

app.listen(port, () => {
  console.log(`Server is listening to port ${port}`);
});
