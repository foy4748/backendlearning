const mongoose = require("mongoose");

const PAYMENT = {
  amount: Number,
  timeStamp: {
    type: Date,
    default: Date.now(),
  },
  user: {
    type: mongoose.Types.ObjectId,
    ref: "USER",
  },
};

module.exports = mongoose.model("PAYMENT", PAYMENT);
