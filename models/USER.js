const mongoose = require("mongoose");

const USER = {
  username: {
    type: String,
    require: true,
  },
  password: {
    type: String,
    require: true,
  },
};

module.exports = mongoose.model("USER", USER);
