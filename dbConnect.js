const mongoose = require("mongoose");
require("dotenv").config();

const URL = process.env.dbURL;

mongoose
  .connect(URL)
  .then(() => console.log("Connected to MongoDB REST API"))
  .catch((error) => console.log(`Something went wrong: \n ${error}`));
