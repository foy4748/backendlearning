//userRegistration page handler
const express = require("express");
const path = require("path");
const router = express.Router();

//Importing necessary modules
const bcrypt = require("bcrypt");
const saltRounds = 10;

//Importing Mongoose model
const USER = require("../models/USER");

router.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "/userRegistration.html"));
});

router.post("/", async (req, res) => {
  USER.findOne({ username: req.body.username }, (error, result) => {
    if (error) {
      console.log(`Something went wrong: \n ${error}`);
    }
    if (result) {
      res.send(`${req.body.username} already exists! Try another username`);
    } else {
      bcrypt.hash(req.body.password, saltRounds, (error, hash) => {
        const userInput = new USER({
          username: req.body.username,
          password: hash,
        });
        userInput
          .save()
          .then((msg) => console.log("Successfully posted \n", msg))
          .catch((error) => console.log(`Something went wrong: \n ${error}`));
        res.send(`Welcome aboard ${req.body.username}`);
      });
    }
  });
});

module.exports = router;
