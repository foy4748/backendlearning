//payment page handler
const express = require("express");
const path = require("path");
const jwtDecoder = require("jwt-decode");
const router = express.Router();

//Importing Mongoose model
const PAYMENT = require("../models/PAYMENT");
const cookieJwtAuth = require("../middlewares/cookieJwtAuth");

router.get("/", cookieJwtAuth, async (req, res) => {
try {
  res.sendFile(path.join(__dirname, "/payment.html"));
	
}
	catch(error)
{
	res.send(`You are not logged in. Please <a href="/userLogin">login here</a>`);
}
});

router.post("/", cookieJwtAuth, (req, res) => {
  const token = req.cookies.token;
  const { userId } = jwtDecoder(token);
  const userInput = new PAYMENT({
    amount: req.body.amount,
    user: userId,
  });

  userInput
    .save()
    .then((msg) => {
      res.send(
        `Payment has been posted successfully <br> ${msg} <br> You may <a href="/logout">logout</a>`
      );
    })
    .catch((error) => console.log(`Something went wrong: \n ${error}`));
});
module.exports = router;
